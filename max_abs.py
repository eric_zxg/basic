s = input("Input a list of number: ")
n = s.split(" ")  # convert string to a list named n
n = [int(i) for i in n] # convert each item from string to a number
min_value = max_value = n[0] # initialize min and max value using the first element

for i in n: # get the min and max value from the list
    if i < min_value:
        min_value = i
    if i > max_value:
        max_value = i

print(min_value, max_value)
print(abs(max_value - min_value))